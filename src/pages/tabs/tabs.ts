import {Component} from '@angular/core';
import {HomePage} from '../home/home';
import {History} from '../history/history';

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {
    tab1Root: any = HomePage;
    tab2Root: any = History;

    constructor() {
    }
}
