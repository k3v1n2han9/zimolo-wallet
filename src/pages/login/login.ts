import {Component} from '@angular/core';
import {Http} from '@angular/http';
import {AlertController, NavController, Platform} from 'ionic-angular';
import {NativeStorage} from 'ionic-native'
import {TabsPage} from "../tabs/tabs";
import * as uuid from 'uuid';
import * as NodeRSA from 'node-rsa';


@Component({
    selector: 'login',
    templateUrl: 'login.html',
})
export class Login {
    phone_number: string;
    valid_phone_number: boolean;
    database_server: string;
    v_num_box_visible: boolean;
    id: any;
    name: string;

    constructor(public navCtrl: NavController,
                public http: Http,
                public platform: Platform,
                public alertCtrl: AlertController) {
        this.database_server = 'http://192.168.1.159:3000';
        this.v_num_box_visible = false;
        platform.ready().then(() => {
            NativeStorage.getItem('phone').then(
                res => {
                    this.phone_number = res;
                    this.navCtrl.setRoot(TabsPage).then();
                },
                err => {
                    this.phone_number = '';
                    console.log(err);
                }
            );

            // this.http.get('http://http://192.168.1.159:3000/get_rsa_key').subscribe(
            //     data => {
            //         console.log(JSON.parse(data['_body'])['public_key_num'].join(','));
            //         console.log(JSON.parse(data['_body'])['private_key_num'].join(','));
            //     }
            // );
        });
    }

    verify_phone_number(input) {
        let valid_area_code = ['604', '778', '236', '250'];
        if (input.includes('+')) {
            input = input.replace('+1', '').trim();
        }
        this.phone_number = input;
        this.valid_phone_number =
            valid_area_code.indexOf(this.phone_number.substring(0, 3)) > -1 &&
            this.phone_number.length == 10;
    }

    send_code() {
        this.http.post(this.database_server + '/user', {
            username: this.name,
            phone_number: this.phone_number,
        }).subscribe(
            data => {
                this.v_num_box_visible = true;
                this.id = JSON.parse(data['_body'])['_id'];
                let public_key_str = JSON.parse(data['_body'])['public_key'];
                NativeStorage.setItem('id', this.id).then();
                NativeStorage.setItem('public_key', public_key_str).then();
                let public_key = new NodeRSA(public_key_str);
                let aes_key = uuid.v4();
                NativeStorage.setItem('aes_key', aes_key).then();
                let encrypted = public_key.encrypt(aes_key, 'base64');
                this.http.post(this.database_server + '/set_key', {
                    'id': this.id,
                    'encrypted': encrypted
                }).subscribe(
                    callback => {
                        console.log(callback);
                    },
                    error => {
                        console.log(error);
                    }
                )
            },
            err => {
                console.log(err);
            }
        );
    }

    user_name(name) {
        this.name = name;
    }

    verify(num, ios) {
        if (('' + num).length == 6) {
            this.http.get(this.database_server + '/verify/' + this.id + '/' + num).subscribe(
                res => {
                    if (res['_body'] == 'true') {
                        NativeStorage.setItem('phone', this.phone_number).then(
                            res => console.log('set phone number ' + res),
                            err => console.log('fail to set phone number')
                        );
                        if (ios) {
                            this.navCtrl.setRoot(TabsPage).then();
                        }
                        else {
                            let alert = this.alertCtrl.create({
                                title: '支付密码',
                                message: '设置一个支付密码，长度至少为四位',
                                inputs: [
                                    {
                                        name: 'pwd1',
                                        placeholder: '密码',
                                        type: 'password',
                                    },
                                    {
                                        name: 'pwd2',
                                        placeholder: '确认密码',
                                        type: 'password'
                                    }
                                ],
                                buttons: [
                                    {
                                        text: '确定',
                                        handler: (data) => {
                                            if (data.pwd1 == data.pwd2) {
                                                NativeStorage.setItem('password', data.pwd1).then();
                                                this.navCtrl.setRoot(TabsPage).then();
                                            }
                                            else {
                                                return false;
                                            }
                                        }
                                    }
                                ]
                            });
                            alert.present().then();
                        }
                    }
                    else {
                        this.v_num_box_visible = false;
                        this.phone_number = '';
                    }
                },
                err => {
                    this.v_num_box_visible = false;
                    this.phone_number = '';
                }
            );

        }
    }
}
