import {Component} from '@angular/core';
import {AlertController, Platform} from 'ionic-angular';
import {CardIO, TouchID, NativeStorage, BarcodeScanner} from "ionic-native";
import {Http} from '@angular/http';
import {ModalController, ViewController, App} from 'ionic-angular';
import {QRModal} from '../modal/qr_modal'
import {NavController} from 'ionic-angular';
import {Login} from "../login/login";
import * as NodeRSA from 'node-rsa';
import * as aes from 'aes256';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    credit_card: Array<{
        id: string,
        object: string,
        address_city: string,
        address_country: string,
        address_line1: string,
        address_line1_check: string,
        address_line2: string,
        address_state: string,
        address_zip: string,
        address_zip_check: string,
        brand: string,
        country: string,
        customer: string,
        cvc_check: string,
        dynamic_last4: null,
        exp_month: number,
        exp_year: number,
        fingerprint: string,
        funding: string,
        last4: string,
        metadata: any,
        name: string,
        tokenization_method: string
    }>;
    id: string;
    public_key: string;
    database_server: string;
    touch_id_available: boolean;
    phone_number: string;
    aes_key: string;

    constructor(public navCtrl: NavController,
                private alertCtrl: AlertController,
                public modalCtrl: ModalController,
                public http: Http,
                public platform: Platform,
                public viewCtrl: ViewController,
                public appCtrl: App) {
        this.database_server = 'http://192.168.1.159:3000';
        this.credit_card = [];
        platform.ready().then(() => {
            NativeStorage.getItem('phone').then(
                res => {
                    this.phone_number = res;
                },
                err => {
                    this.phone_number = '';
                    console.log(err);
                }
            );
            NativeStorage.getItem('id').then(
                res => {
                    this.id = res;
                },
                err => {
                    console.log(err);
                }
            );
            NativeStorage.getItem('public_key').then(
                res => {
                    this.public_key = res;
                },
                err => {
                    console.log(err);
                }
            );
            NativeStorage.getItem('aes_key').then(
                res => {
                    this.aes_key = res;
                    this.update_data();
                },
                err => {
                    console.log(err);
                }
            );

            TouchID.isAvailable().then(
                ok => this.touch_id_available = true,
                no => this.touch_id_available = false,
            );
            this.update_data();
        })
    }

    add_card() {
        CardIO.canScan().then(
            res => {
                let options = {
                    requireExpiry: true,
                    requireCCV: false,
                    requirePostalCode: false,
                    useCardIOLogo: true,
                };
                CardIO.scan(options).then(
                    res1 => {
                        let key = new NodeRSA(this.public_key);
                        let card_info = {
                            'number': res1.cardNumber,
                            'exp_month': res1.expiryMonth,
                            'exp_year': res1.expiryYear,
                            'cvc': res1.cvv
                        };
                        let encrypted_json = {
                            id: this.id,
                            encrypted: key.encrypt(JSON.stringify(card_info), 'base64')
                        };
                        this.http.post(
                            this.database_server + '/user/' + this.id + '/card', encrypted_json).subscribe(data => {
                                console.log(data['_body']);
                                if (!data['_body'].includes('account_balance')) {
                                    if (data['_body'].includes('not supported')) {
                                        let alert = this.alertCtrl.create({
                                            title: '不好意思',
                                            message: '亲，您的信用卡我们暂时不支持哦～～',
                                            buttons: [
                                                {
                                                    text: '知道了',
                                                    role: 'cancel'
                                                }
                                            ]
                                        });
                                        alert.present().then();
                                    }
                                }
                                else {
                                    this.credit_card = [];
                                    JSON.parse(data['_body'])['sources']['data'].forEach(
                                        item => {
                                            this.credit_card.push(item);
                                        }
                                    );
                                }
                            },
                            error => console.log(error)
                        );
                    },
                    err1 => {
                        console.log(err1);
                    }
                );
            },
            err => {
                if (err == 'cordova_not_available') {
                    alert('该设备不支持信用卡扫描');
                }
            }
        );
    }

    remove_card(card) {
        console.log(card);
        let key = new NodeRSA(this.public_key);
        let encrypted_card_id = key.encrypt(card['id'], 'base64');
        this.http.post(this.database_server + '/user/' + this.id + '/card/encrypted', {
            encrypted_card_id: encrypted_card_id
        }).subscribe(
            data => {
                console.log(data['_body']);
            },
            err => {
                console.log(err);
            }
        );
        this.credit_card.forEach(
            (item, index) => {
                if (item['id'] == card['id']) {
                    this.credit_card.splice(index, 1);
                }
                // index++
            }
        );
    }

    update_data() {
        this.http.get(this.database_server + '/user/' + this.id + '/card').subscribe(
            res => {
                this.credit_card = [];
                let encrypted = res['_body'];
                let decrypted_json = JSON.parse(aes.decrypt(this.aes_key, encrypted));
                decrypted_json.forEach(
                    item => {
                        this.credit_card.push(item);
                    }
                );
            },
            err => console.log(err)
        );
    }

    touch_id(card) {
        if (this.platform.is('ios')) {
            TouchID.verifyFingerprint('请按指纹').then(
                data => {
                    this.modalCtrl.create(QRModal, card).present().then();
                },
                err => alert(err)
            )
        }
        else {
            NativeStorage.getItem('password').then(
                pwd => {
                    this.alertCtrl.create({
                        title: '支付密码',
                        message: '请输入您的' + pwd.length + '位数密码',
                        inputs: [
                            {
                                name: 'password',
                                placeholder: '您的密码',
                                type: 'password'
                            }
                        ],
                        buttons: [
                            {
                                text: '取消',
                                role: 'cancel'
                            },
                            {
                                text: '确定',
                                handler: (data) => {
                                    if (data.password == pwd) {
                                        this.modalCtrl.create(QRModal, card).present().then();
                                    }
                                    else {
                                        return true;
                                    }
                                }
                            }
                        ]
                    }).present().then();
                },
                err => {
                    alert(err);
                }
            );
        }

    }

    scan_qr() {
        let cus_alert = this.alertCtrl.create({
            title: 'Zimolo Wallet',
            message: '请输入金额',
            inputs: [
                {
                    name: 'price',
                    placeholder: '例：10.99',
                    type: 'number'
                }
            ],
            buttons: [
                {
                    text: '取消',
                    role: 'cancel'
                },
                {
                    text: '确认',
                    handler: (info) => {
                        BarcodeScanner.scan().then((barcode_data) => {
                            let key = new NodeRSA(this.public_key);

                            let encrypted = key.encrypt(JSON.stringify({
                                amount: info.price,
                                transaction_number: barcode_data['text']
                            }));
                            let request_body = {
                                id: this.id,
                                encrypted: encrypted
                            };
                            this.http.post(this.database_server + '/transaction', request_body).subscribe(data => {
                            });
                        }, (err) => {
                            console.log(err);
                        });
                    }
                }
            ]
        });
        cus_alert.present().then(ok => {
            alert('交易请求已发送');
        });
    }

    logout() {
        let alert = this.alertCtrl.create({
            title: 'Zimolo Wallet',
            message: '确认退出当前账号吗？',
            buttons: [
                {
                    text: '不退出',
                    role: 'cancel'
                },
                {
                    text: '退出',
                    handler: () => {
                        NativeStorage.clear().then(
                            () => {
                                // this.navCtrl.setRoot(Login).then();
                                this.viewCtrl.dismiss().then();
                                this.appCtrl.getRootNav().push(Login).then();
                            }
                        );

                    }
                }
            ]
        });
        alert.present().then();
    }

    show_qr(card) {
        this.modalCtrl.create(QRModal, card).present().then();
    }

    refresh(refresher) {
        setTimeout(
            () => {
                this.update_data();
                refresher.complete();
            }, 1000
        )
    }


}

