import {Component} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';
import {Http} from '@angular/http';
import {NativeStorage} from 'ionic-native';
import moment from 'moment'

@Component({
    selector: 'history',
    templateUrl: 'history.html',
})
export class History {
    testing: string;
    history: Array<{
        description: string;
        amount: string;
        time: any;
        card_number: string;
    }>;
    database_server:string;

    constructor(public navParams: NavParams, public viewCtrl: ViewController, public http: Http) {
        this.history = [];
        this.database_server = 'http://192.168.1.159:3000';
        NativeStorage.getItem('phone').then(
            data => {
                this.http.post(this.database_server + '/methods/history', [data]).subscribe(
                    info => {
                        let obj_array = JSON.parse(info['_body']);
                        let description = '';
                        console.log(obj_array);
                        obj_array.forEach((obj) => {
                            console.log(data);
                            if (obj['payee_phone'] == data) {
                                description = '从' + obj['payer_name'] + '收取';
                            }
                            else {
                                description = '向' + obj['payee_name'] + '支付';
                            }
                            this.history.push({
                                description: description,
                                amount: obj['amount'],
                                time: moment('' + obj['date']).format('MM-DD'),
                                card_number: obj['card_number']
                            })
                        })
                    }
                )
            }
        );
    }

    update_data(){

        this.history=[];
        NativeStorage.getItem('phone').then(
            data => {
                this.http.post(this.database_server + '/methods/history', [data]).subscribe(
                    info => {
                        let obj_array = JSON.parse(info['_body']);
                        let description = '';
                        console.log(obj_array);
                        obj_array.forEach((obj) => {
                            console.log(data);
                            if (obj['payee_phone'] == data) {
                                description = '从' + obj['payer_name'] + '收取';
                            }
                            else {
                                description = '向' + obj['payee_name'] + '支付';
                            }
                            this.history.push({
                                description: description,
                                amount: obj['amount'],
                                time: moment('' + obj['date']).format('MM-DD'),
                                card_number: obj['card_number']
                            })
                        })
                    }
                )
            }
        );

    }
    refresh_history(refresher) {
        setTimeout(
            () => {
                this.update_data();
                refresher.complete();
            }, 1000
        )
    }
}
