import {Component} from '@angular/core';
import {NavParams, ViewController, AlertController} from 'ionic-angular';
import qrcode from 'qrcode-generator'
import moment from 'moment'
import {Http} from '@angular/http';
import {NativeStorage, TouchID} from 'ionic-native'
import * as io from "socket.io-client";
import * as NodeRSA from 'node-rsa';


@Component({
    selector: 'qr-modal',
    templateUrl: 'qr_modal.html',
})
export class QRModal {
    testing: string;
    socket: any;
    id: string;
    touch_id_available: boolean;
    database_server: string;
    public_key: string;
    aes_key: string;

    constructor(public navParams: NavParams,
                public viewCtrl: ViewController,
                public http: Http,
                public alertCtrl: AlertController) {
        let qr = qrcode(4, 'L');
        this.database_server = 'http://192.168.1.159:3000';
        NativeStorage.getItem('public_key').then(
            res => this.public_key = res,
            err => console.log(err)
        );
        NativeStorage.getItem('id').then(
            res => {
                this.id = res;
                this.http.post(this.database_server + '/transaction', {
                    payer_id: res
                }).subscribe(
                    data => {
                        qr.addData(data['_body']);
                        qr.make();
                        this.testing = '<div align="center">' + qr.createImgTag(5) + '</div>';
                    }
                );
            },
            err => {
            }
        );
        NativeStorage.getItem('aes_key').then(
            res => this.aes_key = res,
            err => console.log(err)
        );
        TouchID.isAvailable().then(
            ok => this.touch_id_available = true,
            not_ok => this.touch_id_available = false
        );

        this.socket = io('http://192.168.1.159:3000');
        this.socket.on(this.id, (data) => {
            let cus_alert = alertCtrl.create({
                title: '支付确认',
                message: '确认支付' + data['amount'] + '吗？',
                buttons: [
                    {
                        text: '取消',
                        role: 'cancel'
                    },
                    {
                        text: '确定',
                        handler: (ok) => {

                            // 先扫指纹或者输密码

                            // let key = new NodeRSA(this.public_key);
                            // let encrypted = JSON.stringify({
                            //     confirmation: this.aes_key
                            // });
                            // this.http.put(this.database_server + '/transaction/user', {
                            //     id: this.id,
                            //     encrypted: encrypted
                            // })
                        }
                    }
                ]
            });
            cus_alert.present().then(
                ok => console.log('ok'),
                not_ok => console.log('not ok')
            );
        })
    }

    dismiss() {
        this.viewCtrl.dismiss().then();
    }
}
