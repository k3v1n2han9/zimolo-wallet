import {Component} from '@angular/core';
import {Platform} from 'ionic-angular';
import {StatusBar, Splashscreen} from 'ionic-native';
import {Login} from '../pages/login/login'
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs'
import {History} from '../pages/history/history'

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage = Login ;
    // rootPage = HomePage;
    // rootPage = TabsPage;

    constructor(platform: Platform) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
            Splashscreen.hide();
        });
    }
}
