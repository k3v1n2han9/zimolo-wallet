import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {QRModal} from '../pages/modal/qr_modal';
import {Login} from '../pages/login/login'
import {TabsPage} from '../pages/tabs/tabs'
import {History} from '../pages/history/history'

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        QRModal,
        Login,
        TabsPage,
        History
    ],
    imports: [
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        QRModal,
        Login,
        TabsPage,
        History
    ],
    providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {
}
